from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.

# class UserProfile(models.Model):
#     user = models.ForeignKey(User, on_delete=models.CASCADE)

#     def __str__(self):
#         return f"Profile {self.user.username}"


class Credential(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField()
    title = models.CharField(max_length=50)
    file = models.FileField(upload_to="creds/%Y/%m/%d/")

    def __str__(self):
        return f"CRED: {self.user.email}"

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_at = timezone.now()
        return super(Credential, self).save(*args, **kwargs)


class Spreadsheet(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField()
    title = models.CharField(max_length=50)
    url = models.URLField(blank=True)
    track_sub_times = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.user.email}: {self.title}"

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_at = timezone.now()
        return super(Spreadsheet, self).save(*args, **kwargs)


class SpreadsheetField(models.Model):
    INTEGER = "INTEGER"
    STRING = "STRING"
    BOOL = "BOOL"
    AREA = "AREA"
    FIELD_CHOICES = [
        (INTEGER, "Integer/Number"),
        (STRING, "String/Text Short"),
        (AREA, "String/Text Long"),
        (BOOL, "True/False"),
    ]
    spreadsheet = models.ForeignKey(Spreadsheet, on_delete=models.CASCADE)
    field_name = models.CharField(max_length=50)
    field_type = models.CharField(
        max_length=32,
        choices=FIELD_CHOICES,
        default=STRING,
    )

    def __str__(self):
        return f"SpreadID:{self.spreadsheet.title}| {self.spreadsheet.title}: {self.field_name}"
